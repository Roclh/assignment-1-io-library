section .text
  
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60 
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
  mov rax, -1
.loop:
  inc rax
  cmp byte [rdi + rax], 0
  jne .loop
  ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
  push    rdi
  call string_length
  pop     rsi
  mov     rdx, rax
  mov     rax, 1
  mov     rdi, 1
  syscall
  ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rax, 1
    mov rdi, 1
    syscall
    pop rax
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge print_uint
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rdx, rdx
    mov r10, 10
    push rbp
    mov rbp, rsp
    mov rax, rdi

.loop1:
    cmp rax, 10
    jb .one_digit
    div r10

.general:
    add rdx, '0'
    push rdx
    xor rdx, rdx
    cmp rax, 0
    jne .loop1

    mov rcx, rbp
    sub rcx, rsp
    sar rcx, 3
    mov r9, rsp
    sub rsp, 16
    mov r8, rsp

.loop2:
    mov dil, [r9]
    mov byte[r8], dil
    add r9, 8
    inc r8
    dec rcx
    jnz .loop2

    mov byte[r8], 0
    mov rdi, rsp
    call print_string
    mov rsp, rbp
    pop rbp 
    ret

.one_digit:
    mov rdx, rax
    xor rax, rax
    jmp .general


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rdi
    push rsi
    call string_length
    pop rdi
    push rdi
    push rax
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    jne .ne
.loop:
    mov r8b, [rdi]
    mov r9b, [rsi]
    cmp r8b, r9b
    jne .ne
    cmp r9b, 0x0
    je .eq
    inc rsi
    inc rdi
    jmp .loop
.eq:
    mov rax, 1
    ret

.ne:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    mov rax, 0
    syscall
    cmp rax, 0
    je .end
    pop rax
    ret
.end:
    pop rdi
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rdi    
    push r12
    mov r12, rdi 
    push r13
    mov r13, rsi
.loop_space:
    call read_char
    cmp rax, 0x20
    je .loop_space
    cmp rax, 0x09
    je .loop_space
    cmp rax, 0xA
    je .loop_space
.loop:
    cmp rax, 0x0
    je .finally
    cmp rax, 0x20
    je .finally
    cmp rax, 0x9
    je .finally
    cmp rax, 0xA
    je .finally
    dec r13   
    cmp r13, 0
    jbe .overflow
    mov byte [r12], al
    inc r12
    call read_char
    jmp .loop
    
.finally:
    mov byte [r12], 0
    pop r13      
    pop r12       
    mov rdi, [rsp] 
    call string_length  
    mov rdx, rax 
    pop rax 
    ret

.overflow:
    pop r13 
    pop r12
    pop rdi
    mov rax, 0
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rsi, rsi
    xor r8,  r8
    mov r10, 10
    mov sil, [rdi]
    xor rdx, rdx
    xor rax, rax
    cmp sil, '0'
    jb .Err
    cmp sil, '9'
    ja .Err
.compare:
    cmp sil, 0
    je .End
    cmp sil, '0'
    jb .End
    cmp sil, '9'
    ja .End
.add_num:
    sub sil, '0'
    inc rdi
    inc r8
    mul r10
    add rax, rsi
    mov sil, [rdi]
    jmp .compare
    
.Err:
    mov rdx, 0
    ret
    
.End:
    mov rdx, r8
    ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov sil, [rdi]
    cmp sil, '-'
    je .negative
    call parse_uint
    ret
.negative:
    inc rdi
    call parse_uint
    cmp rax, 0
    je .err
    inc rdx
    neg rax
    ret
.err:
    xor rdx, rdx
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    call string_length
    cmp rdx, rax
    jbe .else 
    mov rcx, rax
.loop:
    test rcx, rcx
    je .end
    dec rcx 
    mov dl, byte [rdi + rcx]
    mov byte [rsi + rcx], dl
    jmp .loop
.end:
    mov byte [rsi + rax], 0
    ret
.else:
    mov rax, 0
    ret
